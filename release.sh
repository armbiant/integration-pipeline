#!/bin/bash
set -exu

# nothing changed
REV_RC=$(git rev-parse origin/release-candidate)
REV_VF=$(git rev-parse origin/verified)
if [[ "$REV_RC" == "$REV_VF" ]]; then
    echo "skip release: nothing has changed"
	exit 0
fi

# throttle releases
LATEST_TAG=$(git tag | grep -E '^v[0-9][0-9]*$' | cut -c2- | sort -rn | head -n 1)
TS_LATEST_TAG=$(git show -s --format=%ct v${LATEST_TAG})
TS_NOW=$(date +"%s")
PASSED=$(( $TS_NOW - $TS_LATEST_TAG ))
# 604800 = 7×24×60×60, one week in seconds
if [ "$PASSED" -lt "604800" ]; then
    echo "skip release: throttled to one release per week"
	exit 0
fi

# tag new release
((NEW_TAG=${LATEST_TAG:0}+1))
git tag v${NEW_TAG} ${REV_RC}
git push origin v${NEW_TAG}
git push origin ${REV_RC}:verified -f
